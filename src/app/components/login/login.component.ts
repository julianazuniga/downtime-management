import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  auth_failed = false;

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.authService.logout();
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get username(): string {
    return this.loginForm.get('username').value;
  }

  get password(): string {
    return this.loginForm.get('password').value;
  }

  login() {
    this.authService
      .login(this.username, this.password).subscribe(
        success => {
          if (success) {
            this.authService.isLicenseActive().subscribe(active => {
              if (active) {
                this.router.navigate(['/home']);
              } else {
                // this.authService.logout();
                this.router.navigate(['/home']);
              }
            });
          } else {
            this.auth_failed = true;
          }
        }
      );
  }

  toggleFail() {
    this.auth_failed = false;
  }
}
