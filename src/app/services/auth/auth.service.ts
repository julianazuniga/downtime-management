import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { DevicewiseApiService } from 'devicewise-angular';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as JsEncryptModule from 'jsencrypt';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  isAuthenticated: string;

  constructor(private devicewise: DevicewiseApiService, private router: Router) {
    this.isAuthenticated = sessionStorage.getItem('isAuthenticated');
  }

  login(username: string, password: string): Observable<boolean> {
    return this.devicewise.login(environment.apiEndpoint, username, password)
      .pipe(
        map(
          response => {
            if (response.success) {
              this.onLoginSuccess();
            } else {
              this.onLoginFail();
            }
            return response.success;
          },
          error => {
            throw error(error);
          }
        )
      );
  }

  isLicenseActive(): Observable<boolean> {
    const encryptService = new JsEncryptModule.JSEncrypt();
    // tslint:disable-next-line:max-line-length
    encryptService.setPrivateKey(
      // tslint:disable-next-line:max-line-length
      '-----BEGIN RSA PRIVATE KEY-----MIIEowIBAAKCAQEAiVZEKm3YkJUXp41tkb7sMeDLIDM0uhJGU5m9DI4RhKRE70q9450mQA8kQaQHLuPsTg2VW+Lx6s95Ravrq9E0TrReIxykdztbz90cNeYXsPanIuQM9s8wYFij/3yNbEn1iAe2m1NzjfvuUtjSZUoT1SLJUqBUSf2KhWyf2qMLgd2qujj6cNjY5yqH9ddZSZjWTIXvM7MCTcxqt2mF60E0bxNW+qXPaSjzMGdL2rj+BarjPuaGrT890xgduc2fzv1mFZE56C+OMUItYz8na40NRha3LyD02HC8Had28KZqQ87ip2u1YFnqHkhjXlWxLpM3uIBQ9bDPWEzogmoxAncVzwIDAQABAoIBAAJINa/CzYNxhsgY+IYgdwpJIvbxAuJIpXSELHkF71KV7DNRuaALn3QXDFIu6JOl4+pUHXQ+bCirs+cteSgMvOLF6n6I3k7A5MCqeuqGXURqhNJ2maSMQtCHCIqZRjxTakoNXsJr+5KTah1kTvKx7WxwPIor8dqc5O1cpbdbWDibMRzJHELDz/z+Wz+HB48ZAOAttSQF0uJDZQPHrZ2o/yn2itA+3uNBSPgq6HCe5pMWmFkwk7A2HWMkSZkK49ag5ltSWu0uNlbm6vyFua4X5puISwNM2RZywxp217CCFJuXS6OqmVyoP9pg2SApN3k/7tUnQwK221obfV+4WBOoCKkCgYEAvHUpMO4i19wg3LQtEqFhzZRnq9egotQ8MFEo4rvJVWgwuxhVpVIIY1Lixp/CCuBeicI+1Pct/0yqQC3YDt+ELLjMkw/hLjo9iocPeK4gmz/lT8E7DBITANyPazreflIBq18ER97fXGJ2DeBJe2Vl8DcqflIR964SCqb6mQl48bMCgYEAuo7S5BqN3R8sZ1ijNA5XeyjRjXj+pfpSGicWs1OGuPQl5M7gPGWaR5cF59a9x6iqv4+2xJE01drmDTCiYGMxh1swurNdxI0sJ8+PT8+fhxRSL8+5JzxpHt/Iyc5Ie2tcFtncfW5LLtmC/oLnY4Q0E8SLnfy/CFWjmSpA3GFXZXUCgYA1OioOJdQ1f3jQnRr13hrPg5DcWiSGmMb/Jo0TrNIqiyjoTvExpj+0Pd7LFqgphkFtSy2P3Nhi+PZE9i3F1Z9xOiidiKii89QZ4heekB+peZx8sCjLOfegEKt5L8FTkDnkewuseOhBIZYSSTy322W1OP+JegiVGdcCpzK0Mpa0PQKBgQCSAsiiCPpxTz20oRYS3wqu9PKVtFYx16o8l81Xiw8NYODrpNKx0p+eWmAf5N1iD+ubxEI6tJ9SpWGafy8HG3ufQcrwxQOlmwwtGHFE+O4Vjd0JWJ3ETK7FoEaJU+880Xkg1WCRtyoWv9ybkB7Dr6wGsLTJcRCe2FATx5M+KN17JQKBgHAaHR+e5dpKs0crBhF9WeZStTmYGZJUQ8BP1YtAhQZh1OyGu0mAxI0jJcQ8sZU6TKMelPwRsaq351yWPGRo/b2EsKtwild5VoC5+LQnxYXNzQMO3AXmYOUr9I17oLkSFa4QvGygH1O0GOjg4bfTlfgmS1QGKvA/x+B2diMeiGKo-----END RSA PRIVATE KEY-----'
    );
    return this.devicewise.subTriggerFire('Demo SC', 'read_license', false, [])
      .pipe(map(license => {
        const exp = encryptService.decrypt(
          license.params['output'][0]['data'][0]
        );
        const expDate = new Date(exp);
        return new Date() < expDate;
      }));
  }

  onLoginSuccess() {
    this.isAuthenticated = 'true';
    window.sessionStorage.setItem('isAuthenticated', this.isAuthenticated);
  }

  onLoginFail() {
    this.isAuthenticated = 'false';
    window.sessionStorage.setItem('isAuthenticated', this.isAuthenticated);
  }

  logout() {
    this.devicewise.logout().subscribe();
    this.isAuthenticated = 'false';
    window.sessionStorage.setItem('isAuthenticated', this.isAuthenticated);
  }
}
