import { Component } from '@angular/core';
import { DevicewiseApiService } from 'devicewise-angular';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'downtime-management';

  constructor(private devicewise: DevicewiseApiService) {
    this.devicewise.setEndpoint(environment.apiEndpoint);
  }
}
