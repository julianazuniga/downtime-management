import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DevicewiseApiService } from 'devicewise-angular';
import { map } from 'rxjs/operators';
import { Line } from '../../models/line.model';

@Injectable({
  providedIn: 'root'
})
export class LinesService {

  constructor(private devicewise: DevicewiseApiService) { }

  getAllLines(): Observable<Line[]> {
    const query = 'SELECT * FROM production_line;';
    return this.devicewise.sql(query).pipe(
      map(response => {
        if (response.params.count !== 0) {
          return response.params.results;
        } else {
          return [];
        }
      })
    );
  }
}
