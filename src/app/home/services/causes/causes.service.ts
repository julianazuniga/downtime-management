import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DevicewiseAngularService, DevicewiseApiService } from 'devicewise-angular';
import { map } from 'rxjs/operators';
import { Cause } from '../../models/cause.model';

@Injectable({
  providedIn: 'root'
})
export class CausesService {

  constructor(private devicewise: DevicewiseApiService) { }

  getAllCauses(): Observable<Cause[]> {
    const query = 'SELECT * FROM stoppage_cause;';
    return this.devicewise.sql(query).pipe(
      map(response => {
        if (response.params.count !== 0) {
          return response.params.results;
        } else {
          return [];
        }
      })
    );
  }
}
