import { TestBed } from '@angular/core/testing';

import { StoppagesService } from './stoppages.service';

describe('StoppagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoppagesService = TestBed.get(StoppagesService);
    expect(service).toBeTruthy();
  });
});
