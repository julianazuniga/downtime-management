import { Injectable } from '@angular/core';
import { DevicewiseApiService } from 'devicewise-angular';
import { Observable } from 'rxjs';
import { Stoppage } from '../../models/stoppage.model';
import { map } from 'rxjs/operators';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
// import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class StoppagesService {
  constructor(private devicewise: DevicewiseApiService) { }

  getAllStoppages(): Observable<Stoppage[]> {
    const query = 'SELECT * FROM line_stoppage;';
    return this.getStoppagesUsingQuery(query);
  }

  getStoppagesByLineAndShift(lineId: number, shiftId: number): Observable<Stoppage[]> {
    const query = `SELECT * FROM line_stoppage WHERE line = ${lineId} AND shift = ${shiftId}`;
    return this.getStoppagesUsingQuery(query);
  }

  getStoppagesByDuration(operator: string, operand: number): Observable<Stoppage[]> {
    const query = `SELECT line_stoppage.*, shift.assignment, stoppage_cause.description, stoppage_cause.description FROM line_stoppage
    LEFT JOIN shift ON line_stoppage.shift = shift.id
    LEFT JOIN stoppage_cause ON line_stoppage.cause = stoppage_cause.id
    WHERE line_stoppage.duration ${operator} ${operand};`;

    return this.getStoppagesUsingQuery(query);
  }

  getStoppagesByDate(fromDate: NgbDate, toDate?: NgbDate): Observable<Stoppage[]> {
    const start = this.dateToString(fromDate);
    const end = toDate ? this.dateToString(toDate) : toDate;

    const query =
      fromDate && toDate
        ? `SELECT line_stoppage.*, shift.assignment, stoppage_cause.description FROM line_stoppage
    LEFT JOIN shift ON line_stoppage.shift = shift.id
    LEFT JOIN stoppage_cause ON line_stoppage.cause = stoppage_cause.id
    WHERE date(line_stoppage.start) >= '${start}' AND date(line_stoppage.start) <= '${end}';`
        : `SELECT line_stoppage.*, shift.assignment, stoppage_cause.description FROM line_stoppage
    LEFT JOIN shift ON line_stoppage.shift = shift.id
    LEFT JOIN stoppage_cause ON line_stoppage.cause = stoppage_cause.id
    WHERE date(line_stoppage.start) = '${start}';`;

    return this.getStoppagesUsingQuery(query);
  }

  getStoppagesByDurationAndDate(operator: string, operand: number, fromDate: NgbDate, toDate?: NgbDate): Observable<Stoppage[]> {
    const start = this.dateToString(fromDate);
    const end = toDate ? this.dateToString(toDate) : toDate;

    const query =
      fromDate && toDate ?
        `SELECT line_stoppage.*, shift.assignment, stoppage_cause.description FROM line_stoppage
      LEFT JOIN shift ON line_stoppage.shift = shift.id
      LEFT JOIN stoppage_cause ON line_stoppage.cause = stoppage_cause.id
      WHERE line_stoppage.duration ${operator} ${operand} AND
      date(line_stoppage.start) >= '${start}' AND date(line_stoppage.start) <= '${end}';`
        : `SELECT line_stoppage.*, shift.assignment, stoppage_cause.description FROM line_stoppage
      LEFT JOIN shift ON line_stoppage.shift = shift.id
      LEFT JOIN stoppage_cause ON line_stoppage.cause = stoppage_cause.id
      WHERE line_stoppage.duration ${operator} ${operand} AND date(line_stoppage.start) = '${start}';`;

    return this.getStoppagesUsingQuery(query);
  }

  private getStoppagesUsingQuery(query: string): Observable<Stoppage[]> {
    return this.devicewise.sql(query).pipe(
      map(response => {
        if (response.params.count !== 0) {
          return response.params.results;
        } else {
          return null;
        }
      })
    );
  }

  private dateToString(date: NgbDate): string {
    const month = (date.month + '').padStart(2, '0');
    const day = (date.day + '').padStart(2, '0');
    return date.year + '-' + month + '-' + day;
  }

  updateStopCause(stoppageId: number, causeId: number): Observable<boolean> {
    const updateCauseQuery = `UPDATE line_stoppage SET cause = ${causeId} WHERE id = ${stoppageId}`;
    return this.devicewise.sql(updateCauseQuery).pipe(
      map(
        response => response.success
      )
    );
  }

}
