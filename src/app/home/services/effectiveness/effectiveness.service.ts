import { Injectable } from '@angular/core';
import { DevicewiseAngularService, DevicewiseApiService } from 'devicewise-angular';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Effectiveness } from '../../models/effectiveness.model';
import { Stoppage } from '../../models/stoppage.model';

@Injectable({
  providedIn: 'root'
})
export class EffectivenessService {

  constructor(private devicewise: DevicewiseApiService) { }

  getEffectiveness(shiftId: number, lineId: number): Observable<Effectiveness> {
    const query = `SELECT * FROM effectiveness WHERE shift = ${shiftId} AND line = ${lineId};`;
    return this.devicewise.sql(query).pipe(
      map(response => {
        if (response.params.count !== 0) {
          return response.params.results[0];
        } else {
          return null;
        }
      })
    );
  }

  saveEffectiveness(
    shiftId: number, lineId: number, downtime: number,
    prodTarget: number, totalProd: number, defectiveProd: number,
    saleableProd: number, ole: number): Observable<boolean> {
    const query = `INSERT INTO effectiveness
    ("shift", "line", "downtime", "production_target", "total_production", "defective_production", "saleable_production", "ole")
    VALUES (${shiftId}, ${lineId}, ${downtime}, ${prodTarget}, ${totalProd}, ${defectiveProd}, ${saleableProd}, ${ole})`;
    return this.devicewise.sql(query).pipe(
      map(
        response => response.success
      )
    );
  }

  getDowntime(lineId: number, shiftId: number): Promise<number> {
    const query = `SELECT * FROM line_stoppage WHERE line = ${lineId} AND shift = ${shiftId}`;
    let downtime: number;
    return new Promise<number>((resolve, reject) => {
      this.devicewise.sql(query)
        .pipe(take(1))
        .subscribe(
          response => {
            downtime = 0;
            if (response.params.count !== 0) {
              response.params.results.forEach(
                (stoppage: Stoppage) => {
                  downtime = downtime + Number(stoppage.duration);
                }
              );
              resolve(downtime / 60);
            }
          },
          error => {
            console.log(error);
            reject(error);
          }
        );
    });
  }

}
