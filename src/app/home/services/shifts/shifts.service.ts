import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DevicewiseApiService } from 'devicewise-angular';
import { map } from 'rxjs/operators';
import { Shift } from '../../models/shift.model';

@Injectable({
  providedIn: 'root'
})
export class ShiftsService {

  constructor(private devicewise: DevicewiseApiService) { }

  getAllShifts(): Observable<Shift[]> {
    const query = 'SELECT * FROM shift;';
    return this.devicewise.sql(query).pipe(
      map(
        response => {
          if (response.params.count !== 0) {
            return response.params.results;
          } else {
            return [];
          }
        }
      )
    );
  }

  getShift(date: string, assignment: number): Observable<Shift> {
    const query = `SELECT * FROM shift WHERE date(start) = '${date}' AND assignment = ${assignment};`;
    return this.devicewise.sql(query).pipe(
      map(response => {
        if (response.success && response.params.count > 0) {
          return response.params.results[0];
        } else {
          return null;
        }
      })
    );
  }
}
