import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import * as _ from 'lodash';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Observable, Subject, of } from 'rxjs';
import { StoppagesService } from '../../../services/stoppages/stoppages.service';
import { takeUntil, tap } from 'rxjs/operators';
import { Stoppage } from 'src/app/home/models/stoppage.model';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit, OnDestroy {
  operators = {
    1: {
      symbol: '<=',
      meaning: 'less than or equal to'
    },
    2: {
      symbol: '>=',
      meaning: 'greater than or equal to'
    },
    3: {
      symbol: '=',
      meaning: 'equal to'
    }
  };

  hoveredDate: NgbDate;

  shiftLabels = ['Shift 1', 'Shift 2', 'Shift 3'];

  destroy$: Subject<any> = new Subject();

  filterForm: FormGroup;
  stoppages$: Observable<Stoppage[]>;
  shiftPercentages = [];

  constructor(private formBuilder: FormBuilder, private stoppagesService: StoppagesService) {
  }

  ngOnInit(): void {
    this.filterForm = this.formBuilder.group({
      operator: [''],
      operand: [null],
      fromDate: [''],
      toDate: ['']
    });

    this.filterForm.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(
      chages => {
        if (this.isFormValid()) {
          this.stoppages$ = this.getStoppages().pipe(
            tap(stoppages => {
              if (stoppages.length > 0) {
                this.calculateShiftPercentages(stoppages);
              } else {
                this.shiftPercentages = [];
              }
            })
          );
        }
      }
    );
  }

  isFormValid(): boolean {
    return !!this.operator && this.operand !== null;
  }

  get operator(): string {
    return this.filterForm.get('operator').value;
  }

  get operand(): number {
    return this.filterForm.get('operand').value;
  }

  get fromDate(): NgbDate {
    return this.filterForm.get('fromDate').value;
  }

  get toDate(): NgbDate {
    return this.filterForm.get('toDate').value;
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.filterForm.get('fromDate').setValue(date);
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.filterForm.get('toDate').setValue(date);
    } else {
      this.filterForm.get('toDate').setValue(null);
      this.filterForm.get('fromDate').setValue(date);
    }
  }

  getStoppages(): Observable<Stoppage[]> {
    if (!!this.operator && this.operand !== null && !(this.fromDate || this.toDate)) {
      // filter by duration
      return this.stoppagesService.getStoppagesByDuration(this.operator, this.operand);
    } else if (!this.operator && this.operand === null && ((this.fromDate || this.toDate))) {
      // filter by date
      return this.stoppagesService.getStoppagesByDate(this.fromDate, this.toDate);
    } else if (!!this.operator && this.operand !== null && (this.fromDate || this.toDate)) {
      // filter by duration and date
      return this.stoppagesService.getStoppagesByDurationAndDate(this.operator, this.operand, this.fromDate, this.toDate);
    } else {
      return of([]);
    }
  }

  calculateShiftPercentages(stoppages: Array<Stoppage>) {
    const result = _.groupBy(stoppages, stoppage => stoppage.assignment);
    const type1QTY = result['1'] === undefined ? 0 : result['1'].length;
    const type2QTY = result['2'] === undefined ? 0 : result['2'].length;
    const type3QTY = result['3'] === undefined ? 0 : result['3'].length;
    this.shiftPercentages = [type1QTY, type2QTY, type3QTY];
  }

  // Get the tooltip to show in the chart (show percentages)
  customTooltip(tooltipItem, data): string {
    const values = data.datasets[0].data;
    const total = parseFloat(values[0]) + parseFloat(values[1]) + parseFloat(values[2]);
    const currentValue = values[tooltipItem.index];
    const percentage = Math.floor((currentValue / total) * 100);
    return percentage + '%';
  }

  /**
   * Datepicker methods
   */
  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
