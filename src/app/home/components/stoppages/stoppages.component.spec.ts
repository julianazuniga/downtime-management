import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoppagesComponent } from './stoppages.component';

describe('StoppagesComponent', () => {
  let component: StoppagesComponent;
  let fixture: ComponentFixture<StoppagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoppagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoppagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
