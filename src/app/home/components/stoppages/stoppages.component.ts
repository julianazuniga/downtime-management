import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { takeUntil, take, tap } from 'rxjs/operators';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Line } from '../../models/line.model';
import { Shift } from '../../models/shift.model';
import { Cause } from '../../models/cause.model';
import { Stoppage } from '../../models/stoppage.model';
import { LinesService } from '../../services/lines/lines.service';
import { CausesService } from '../../services/causes/causes.service';
import { StoppagesService } from '../../services/stoppages/stoppages.service';
import { ShiftsService } from '../../services/shifts/shifts.service';

@Component({
  selector: 'app-stoppages',
  templateUrl: './stoppages.component.html',
  styleUrls: ['./stoppages.component.css']
})
export class StoppagesComponent implements OnInit, OnDestroy {

  @ViewChild('confirmAlert', { static: true }) private confirmAlert: SwalComponent;

  destroy$: Subject<any>;

  lines$: Observable<Line[]>;
  shifts$: Observable<Shift[]>;
  causes$: Observable<Cause[]>;

  stoppages$: Observable<Stoppage[]>;
  stoppages: Stoppage[];

  filterForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private linesService: LinesService,
    private shiftsService: ShiftsService,
    private causesService: CausesService,
    private stoppagesService: StoppagesService
  ) { }

  ngOnInit() {

    this.filterForm = this.formBuilder.group({
      line: ['', Validators.required],
      shift: ['', Validators.required]
    });

    this.destroy$ = new Subject();

    this.filterForm.statusChanges
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(
        change => this.getStoppages()
      );

    this.lines$ = this.linesService.getAllLines();
    this.shifts$ = this.shiftsService.getAllShifts();
    this.causes$ = this.causesService.getAllCauses();
  }

  get selectedLine(): Line {
    return this.filterForm.get('line').value;
  }

  get selectedShift(): Shift {
    return this.filterForm.get('shift').value;
  }

  getStoppages() {
    if (this.filterForm.valid) {
      this.stoppages$ = this.stoppagesService.getStoppagesByLineAndShift(
        this.selectedLine.id,
        this.selectedShift.id
      ).pipe(
        tap(
          stoppages => this.stoppages = stoppages
        )
      );
    }
  }

  updateStoppageCause(index: string) {
    this.stoppagesService.updateStopCause(this.stoppages[index].id, this.stoppages[index].cause).pipe(
      take(1)
    ).subscribe(
      success => {
        if (success) {
          this.confirmAlert.fire();
        }
      }
    );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
