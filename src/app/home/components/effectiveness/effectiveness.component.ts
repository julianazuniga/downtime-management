import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { LinesService } from '../../services/lines/lines.service';
import { ShiftsService } from '../../services/shifts/shifts.service';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
import { Line } from '../../models/line.model';
import { Shift } from '../../models/shift.model';
import { EffectivenessService } from '../../services/effectiveness/effectiveness.service';

import { faCalendarAlt, faSearch, faSave } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-effectiveness',
  templateUrl: './effectiveness.component.html',
  styleUrls: ['./effectiveness.component.css']
})
export class EffectivenessComponent implements OnInit {

  @ViewChild('noDataAlert', {static: true}) private noDataAlert: SwalComponent;
  @ViewChild('savedAlert', {static: true}) private savedAlert: SwalComponent;
  @ViewChild('errorAlert', {static: true}) private errorAlert: SwalComponent;

  faCalendar = faCalendarAlt;
  faSearch = faSearch;
  faSave = faSave;

  filterForm: FormGroup;

  lines$: Observable<Line[]>;
  assignments = [1, 2, 3];

  ole: number;

  scheduledTime: number;
  availableTime: number;
  availability = 0;

  expectedProduction: number;
  realProduction: number;
  performance = 0;

  saleableParts: number;
  quality = 0;

  constructor(
    private linesService: LinesService,
    private shiftsService: ShiftsService,
    private effectivenessService: EffectivenessService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.filterForm = this.formBuilder.group({
      line: [''],
      date: [''],
      assignment: [''],
      shift: ['']
    });
    this.lines$ = this.linesService.getAllLines();
  }

  get selectedLine(): Line {
    return this.filterForm.get('line').value;
  }

  get selectedDate(): string {
    return this.filterForm.get('date').value;
  }

  get selectedAssignment(): number {
    return this.filterForm.get('assignment').value;
  }

  get selectedShift(): Shift {
    return this.filterForm.get('shift').value;
  }

  setSelectedDate(date: NgbDate) {
    const month = `${date.month}`.padStart(2, '0');
    const day = `${date.day}`.padStart(2, '0');
    this.filterForm.get('date').setValue(`${date.year}-${month}-${day}`);
  }

  setSelectedShift() {
    this.shiftsService.getShift(this.selectedDate, this.selectedAssignment)
      .pipe(take(1))
      .subscribe(
        shift => {
          if (shift) {
            this.filterForm.get('shift').setValue(shift);
          } else {
            this.noDataAlert.fire();
          }
        }, error => {
          console.log(error);
        }
      );
  }

  getOLE() {
    this.effectivenessService.getEffectiveness(this.selectedShift.id, this.selectedLine.id).subscribe(
      effectiveness => {
        this.scheduledTime = this.selectedShift.duration;
        if (effectiveness) {

          this.ole = effectiveness.ole;

          this.availableTime = this.scheduledTime - effectiveness.downtime;
          this.calculateAvailability();

          this.expectedProduction = effectiveness.production_target;
          this.realProduction = effectiveness.total_production;
          this.calculatePerformance();

          this.saleableParts = effectiveness.saleable_production;
          this.calculateQuality();
        } else {
          this.calculateAvailableTime();

          this.expectedProduction = this.selectedLine.speed * this.scheduledTime;
          this.calculatePerformance();

          this.calculateQuality();

          this.calculateOLE();
        }
      }, error => {
        console.log(error);
      }
    );
  }

  calculateAvailableTime() {
    this.effectivenessService.getDowntime(this.selectedLine.id, this.selectedShift.id).
      then(downtime => {
        this.availableTime = this.scheduledTime - downtime;
        this.calculateAvailability();
      }).catch(error =>
        console.log(error)
      );
  }

  calculateAvailability() {
    this.availability = (this.availableTime / this.scheduledTime) * 100;
  }

  calculatePerformance() {
    this.performance = (this.realProduction / this.expectedProduction) * 100;
  }

  calculateQuality() {
    this.quality = (this.saleableParts / this.realProduction) * 100;
  }

  calculateOLE() {
    this.ole = (this.availability + this.performance + this.quality) / 3;
  }

  saveEffectiveness() {
    this.effectivenessService.saveEffectiveness(
      this.selectedShift.id,
      this.selectedLine.id,
      this.scheduledTime - this.availableTime,
      this.expectedProduction,
      this.realProduction,
      this.expectedProduction - this.saleableParts,
      this.saleableParts,
      this.ole
    ).pipe(take(1)).subscribe(
      success => {
        if (success) {
          this.savedAlert.fire();
        } else {
          this.errorAlert.fire();
        }
      },
      error => this.errorAlert.fire()
    );
  }

}
