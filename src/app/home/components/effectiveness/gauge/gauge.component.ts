import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.css']
})
export class GaugeComponent implements OnInit {

  type = 'arch';
  thick = 8;
  cap = 'butt';
  append = '%';

  @Input()
  value: number;

  @Input()
  label: string;

  @Input()
  size = 300;

  constructor() { }

  ngOnInit() {
  }

}
