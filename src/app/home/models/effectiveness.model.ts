export class Effectiveness {
  constructor(
    public id: number,
    public shift: number,
    public line: number,
    public downtime: number,
    public production_target: number,
    public total_production: number,
    public defective_production: number,
    public saleable_production: number,
    public ole: number) { }
}
