export class Line {
  public id: number;
  public name: string;
  public speed: number;

  constructor(id: number, name: string, speed: number) {
    this.id = id;
    this.name = name;
    this.speed = speed;
  }
}
