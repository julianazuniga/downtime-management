export class Cause {
  public id: number;
  public code: string;
  public description: string;
  public affects_ole: boolean;

  constructor(id: number, code: string, description: string, affects_ole: boolean) {
    this.id = id;
    this.code = code;
    this.description = description;
    this.affects_ole = affects_ole;
  }
}
