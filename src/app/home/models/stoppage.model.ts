export class Stoppage {
  public id: number;
  public start: Date;
  public end: Date;
  public duration: number;
  public cause: number;
  public line: number;
  public shift: number;
  public assignment?: number;
  public description?: string;

  constructor(
    id: number,
    start: Date,
    end: Date,
    duration: number,
    cause: number,
    line: number,
    shift: number,
    assignment?: number,
    description?: string
  ) {
    this.id = id;
    this.start = start;
    this.end = end;
    this.duration = duration;
    this.cause = cause;
    this.line = line;
    this.shift = shift;
    this.assignment = assignment;
    this.description = description;
  }
}
