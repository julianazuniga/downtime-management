export class Shift {
  public id: number;
  public start: Date;
  public end: Date;
  public duration: number;
  public assignment: number;

  constructor(id: number, start: Date, end: Date, duration: number, assignment: number) {
    this.id = id;
    this.start = start;
    this.end = end;
    this.duration = duration;
    this.assignment = assignment;
  }
}
