import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeContainerComponent } from './components/home-container/home-container.component';
import { StoppagesComponent } from './components/stoppages/stoppages.component';
import { ReportsComponent } from './components/reports/reports/reports.component';
import { EffectivenessComponent } from './components/effectiveness/effectiveness.component';


const routes: Routes = [
  { path: '', component: HomeContainerComponent, children: [
    { path: 'stoppages', component: StoppagesComponent },
    { path: 'reports', component: ReportsComponent },
    { path: 'effectiveness', component: EffectivenessComponent },
    { path: '', redirectTo: 'stoppages', pathMatch: 'full' },
    { path: '**', redirectTo: 'stoppages', pathMatch: 'full' }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
