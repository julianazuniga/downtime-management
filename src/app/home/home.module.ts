import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { NgxGaugeModule } from 'ngx-gauge';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { HomeRoutingModule } from './home-routing.module';
import { HomeContainerComponent } from './components/home-container/home-container.component';
import { NavbarComponent } from './components/navbar/navbar/navbar.component';
import { StoppagesComponent } from './components/stoppages/stoppages.component';
import { ReportsComponent } from './components/reports/reports/reports.component';
import { EffectivenessComponent } from './components/effectiveness/effectiveness.component';
import { GaugeComponent } from './components/effectiveness/gauge/gauge.component';

@NgModule({
  declarations: [HomeContainerComponent, StoppagesComponent, NavbarComponent, ReportsComponent, EffectivenessComponent, GaugeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    SweetAlert2Module,
    NgbModule,
    ChartsModule,
    NgxGaugeModule,
    FontAwesomeModule
  ],
  providers: []
})
export class HomeModule { }
